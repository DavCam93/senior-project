﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;

namespace ShowStarterSystem2.Controllers
{
    public class VenueController : Controller
    {
        private ShowStarterDBEntities db = new ShowStarterDBEntities();

        // GET: Venue
        public ActionResult Index()
        {
            return View(db.VENUEs.ToList());
        }

        // GET: Venue/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VENUE vENUE = db.VENUEs.Find(id);
            if (vENUE == null)
            {
                return HttpNotFound();
            }
            return View(vENUE);
        }

        // GET: Venue/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Venue/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VenueID,VenueName,StreetAddress,City,StateAddress,ZIPCode,ContactName,ContactPhone,ContactEmail")] VENUE V)
        {
            if (ModelState.IsValid)
            {
				db.usp_AddVenue(V.VenueName, V.StreetAddress, V.City, V.StateAddress, V.ZIPCode, V.ContactName, V.ContactPhone, V.ContactEmail);
				return RedirectToAction("Index");
            }

            return View(V);
        }

        // GET: Venue/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VENUE vENUE = db.VENUEs.Find(id);
            if (vENUE == null)
            {
                return HttpNotFound();
            }
            return View(vENUE);
        }

        // POST: Venue/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VenueID,VenueName,StreetAddress,City,StateAddress,ZIPCode,ContactName,ContactPhone,ContactEmail")] VENUE vENUE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vENUE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vENUE);
        }

        // GET: Venue/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VENUE vENUE = db.VENUEs.Find(id);
            if (vENUE == null)
            {
                return HttpNotFound();
            }
            return View(vENUE);
        }

        // POST: Venue/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VENUE vENUE = db.VENUEs.Find(id);
            db.VENUEs.Remove(vENUE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
