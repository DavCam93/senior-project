﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Web.Security;

namespace ShowStarterSystem.Controllers
{
	[AllowAnonymous]
	public class AccountController : Controller
	{

		public ActionResult Logout()
		{
			FormsAuthentication.SignOut();
			return RedirectToAction("Index", "Home");
		}
		// GET: Account
		public ActionResult Login()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Login(USER U)
		{
			ShowStarterDBEntities db = new ShowStarterDBEntities();
			

			var passToSend = CreateSHAHash(U.RawPassword);

			bool doesExist = db.uf_UserLogin2(U.UserName, passToSend).FirstOrDefault() ?? false;
			

			if (!doesExist)
			{
				ViewBag.Msg = "Invalid User";
				return View();
			}
			else
			{
				FormsAuthentication.SetAuthCookie(U.UserName, false);
				return RedirectToAction("Index", "Home");
			}

		}


		public ActionResult Register()
		{
			return View();
		}

		[HttpPost]
		public ActionResult Register(USER U)
		{
			var passToSend = CreateSHAHash(U.RawPassword);

			ShowStarterDBEntities db = new ShowStarterDBEntities();
			var count = db.USERS.Where(x => x.UserName == U.UserName).Count();

			if (count == 1)
			{
				ViewBag.Msg = "Username already taken";
				return View();
			}

			db.usp_AddUser(U.UserName, U.Email, U.LegalFirstName, U.LegalLastName, passToSend);

			ViewBag.Msg = "Please Login To Continue";
			return RedirectToAction("Login", "Account");
		}

		private static string CreateSHAHash(string Phrase)
		{
			SHA512Managed HashTool = new SHA512Managed();
			Byte[] PhraseAsByte = System.Text.Encoding.UTF8.GetBytes(string.Concat(Phrase));
			Byte[] EncryptedBytes = HashTool.ComputeHash(PhraseAsByte);
			HashTool.Clear();
			return Convert.ToBase64String(EncryptedBytes);
		}
	}
}