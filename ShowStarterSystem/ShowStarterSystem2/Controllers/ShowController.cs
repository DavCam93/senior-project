﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;

namespace ShowStarterSystem2.Controllers
{
    public class ShowController : Controller
    {
        private ShowStarterDBEntities db = new ShowStarterDBEntities();

        // GET: Show
        public ActionResult Index()
        {
            return View(db.SHOWs.ToList());
        }

        // GET: Show/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SHOW sHOW = db.SHOWs.Find(id);
            if (sHOW == null)
            {
                return HttpNotFound();
            }
            return View(sHOW);
        }

        // GET: Show/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Show/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ShowID,ShowName,StartDate,EndDate,Theme,isDefaultShow")] SHOW sHOW)
        {
            if (ModelState.IsValid)
            {
                db.SHOWs.Add(sHOW);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sHOW);
        }

        // GET: Show/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SHOW sHOW = db.SHOWs.Find(id);
            if (sHOW == null)
            {
                return HttpNotFound();
            }
            return View(sHOW);
        }

        // POST: Show/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ShowID,ShowName,StartDate,EndDate,Theme,isDefaultShow")] SHOW sHOW)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sHOW).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sHOW);
        }

        // GET: Show/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SHOW sHOW = db.SHOWs.Find(id);
            if (sHOW == null)
            {
                return HttpNotFound();
            }
            return View(sHOW);
        }

        // POST: Show/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SHOW sHOW = db.SHOWs.Find(id);
            db.SHOWs.Remove(sHOW);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
