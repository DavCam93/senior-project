﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;

namespace ShowStarterSystem2.Controllers
{
    public class InventoryController : Controller
    {
        private ShowStarterDBEntities db = new ShowStarterDBEntities();

        // GET: Inventory
        public ActionResult Index()
        {
            var iNVENTORY_ITEM = db.INVENTORY_ITEM.Include(i => i.DEPARTMENT1);
            return View(iNVENTORY_ITEM.ToList());
        }

        // GET: Inventory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVENTORY_ITEM iNVENTORY_ITEM = db.INVENTORY_ITEM.Find(id);
            if (iNVENTORY_ITEM == null)
            {
                return HttpNotFound();
            }
            return View(iNVENTORY_ITEM);
        }

        // GET: Inventory/Create
        public ActionResult Create()
        {
            ViewBag.Department = new SelectList(db.DEPARTMENTs, "DepartmentID", "DepartmentName");
            return View();
        }

        // POST: Inventory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InventoryID,ShortDescription,LongDescription,QuantityOnHand,Department")] INVENTORY_ITEM iNVENTORY_ITEM)
        {
            if (ModelState.IsValid)
            {
                db.INVENTORY_ITEM.Add(iNVENTORY_ITEM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Department = new SelectList(db.DEPARTMENTs, "DepartmentID", "DepartmentName", iNVENTORY_ITEM.Department);
            return View(iNVENTORY_ITEM);
        }

        // GET: Inventory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVENTORY_ITEM iNVENTORY_ITEM = db.INVENTORY_ITEM.Find(id);
            if (iNVENTORY_ITEM == null)
            {
                return HttpNotFound();
            }
            ViewBag.Department = new SelectList(db.DEPARTMENTs, "DepartmentID", "DepartmentName", iNVENTORY_ITEM.Department);
            return View(iNVENTORY_ITEM);
        }

        // POST: Inventory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InventoryID,ShortDescription,LongDescription,QuantityOnHand,Department")] INVENTORY_ITEM iNVENTORY_ITEM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iNVENTORY_ITEM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Department = new SelectList(db.DEPARTMENTs, "DepartmentID", "DepartmentName", iNVENTORY_ITEM.Department);
            return View(iNVENTORY_ITEM);
        }

        // GET: Inventory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INVENTORY_ITEM iNVENTORY_ITEM = db.INVENTORY_ITEM.Find(id);
            if (iNVENTORY_ITEM == null)
            {
                return HttpNotFound();
            }
            return View(iNVENTORY_ITEM);
        }

        // POST: Inventory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            INVENTORY_ITEM iNVENTORY_ITEM = db.INVENTORY_ITEM.Find(id);
            db.INVENTORY_ITEM.Remove(iNVENTORY_ITEM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
