﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;

namespace ShowStarterSystem2.Controllers
{
    public class EventController : Controller
    {
        private ShowStarterDBEntities db = new ShowStarterDBEntities();

        // GET: Event
        public ActionResult Index()
        {
            var sHOW_EVENT = db.SHOW_EVENT.Include(s => s.ROOM_CONFIGURATION).Include(s => s.SHOW);
            return View(sHOW_EVENT.ToList());
        }

        // GET: Event/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SHOW_EVENT sHOW_EVENT = db.SHOW_EVENT.Find(id);
            if (sHOW_EVENT == null)
            {
                return HttpNotFound();
            }
            return View(sHOW_EVENT);
        }

        // GET: Event/Create
        public ActionResult Create()
        {
            ViewBag.RoomConfiguration = new SelectList(db.ROOM_CONFIGURATION, "ConfigurationID", "ConfigurationName");
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName");
            return View();
        }

        // POST: Event/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EventID,EventName,EventDescription,RequestedEquipment,RoomConfiguration,ShowID,StartTime,EndTime,ExpectedAttendance,ActualAttendance,WasInPreviousShow")] SHOW_EVENT sHOW_EVENT)
        {
            if (ModelState.IsValid)
            {
                db.SHOW_EVENT.Add(sHOW_EVENT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RoomConfiguration = new SelectList(db.ROOM_CONFIGURATION, "ConfigurationID", "ConfigurationName", sHOW_EVENT.RoomConfiguration);
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName", sHOW_EVENT.ShowID);
            return View(sHOW_EVENT);
        }

        // GET: Event/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SHOW_EVENT sHOW_EVENT = db.SHOW_EVENT.Find(id);
            if (sHOW_EVENT == null)
            {
                return HttpNotFound();
            }
            ViewBag.RoomConfiguration = new SelectList(db.ROOM_CONFIGURATION, "ConfigurationID", "ConfigurationName", sHOW_EVENT.RoomConfiguration);
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName", sHOW_EVENT.ShowID);
            return View(sHOW_EVENT);
        }

        // POST: Event/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EventID,EventName,EventDescription,RequestedEquipment,RoomConfiguration,ShowID,StartTime,EndTime,ExpectedAttendance,ActualAttendance,WasInPreviousShow")] SHOW_EVENT sHOW_EVENT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sHOW_EVENT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RoomConfiguration = new SelectList(db.ROOM_CONFIGURATION, "ConfigurationID", "ConfigurationName", sHOW_EVENT.RoomConfiguration);
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName", sHOW_EVENT.ShowID);
            return View(sHOW_EVENT);
        }

        // GET: Event/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SHOW_EVENT sHOW_EVENT = db.SHOW_EVENT.Find(id);
            if (sHOW_EVENT == null)
            {
                return HttpNotFound();
            }
            return View(sHOW_EVENT);
        }

        // POST: Event/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SHOW_EVENT sHOW_EVENT = db.SHOW_EVENT.Find(id);
            db.SHOW_EVENT.Remove(sHOW_EVENT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
