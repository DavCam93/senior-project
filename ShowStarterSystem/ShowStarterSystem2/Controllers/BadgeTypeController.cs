﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;

namespace ShowStarterSystem2.Controllers
{
    public class BadgeTypeController : Controller
    {
        private ShowStarterDBEntities db = new ShowStarterDBEntities();

        // GET: BADGE_TYPE
        public ActionResult Index()
        {
            var bADGE_TYPE = db.BADGE_TYPE.Include(b => b.SHOW);
            return View(bADGE_TYPE.ToList());
        }

        // GET: BADGE_TYPE/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BADGE_TYPE bADGE_TYPE = db.BADGE_TYPE.Find(id);
            if (bADGE_TYPE == null)
            {
                return HttpNotFound();
            }
            return View(bADGE_TYPE);
        }

        // GET: BADGE_TYPE/Create
        public ActionResult Create()
        {
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName");
            return View();
        }

        // POST: BADGE_TYPE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BadgeTypeID,ShowID,BadgeName,BadgePrice,IsAvailable,BadgeDays")] BADGE_TYPE bADGE_TYPE)
        {
            if (ModelState.IsValid)
            {
                db.BADGE_TYPE.Add(bADGE_TYPE);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName", bADGE_TYPE.ShowID);
            return View(bADGE_TYPE);
        }

        // GET: BADGE_TYPE/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BADGE_TYPE bADGE_TYPE = db.BADGE_TYPE.Find(id);
            if (bADGE_TYPE == null)
            {
                return HttpNotFound();
            }
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName", bADGE_TYPE.ShowID);
            return View(bADGE_TYPE);
        }

        // POST: BADGE_TYPE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BadgeTypeID,ShowID,BadgeName,BadgePrice,IsAvailable,BadgeDays")] BADGE_TYPE bADGE_TYPE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bADGE_TYPE).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ShowID = new SelectList(db.SHOWs, "ShowID", "ShowName", bADGE_TYPE.ShowID);
            return View(bADGE_TYPE);
        }

        // GET: BADGE_TYPE/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BADGE_TYPE bADGE_TYPE = db.BADGE_TYPE.Find(id);
            if (bADGE_TYPE == null)
            {
                return HttpNotFound();
            }
            return View(bADGE_TYPE);
        }

        // POST: BADGE_TYPE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BADGE_TYPE bADGE_TYPE = db.BADGE_TYPE.Find(id);
            db.BADGE_TYPE.Remove(bADGE_TYPE);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
