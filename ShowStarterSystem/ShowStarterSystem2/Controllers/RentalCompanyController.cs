﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;

namespace ShowStarterSystem2.Controllers
{
    public class RentalCompanyController : Controller
    {
        private ShowStarterDBEntities db = new ShowStarterDBEntities();

        // GET: RentalCompany
        public ActionResult Index()
        {
            return View(db.RENTAL_COMPANY.ToList());
        }

        // GET: RentalCompany/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RENTAL_COMPANY rENTAL_COMPANY = db.RENTAL_COMPANY.Find(id);
            if (rENTAL_COMPANY == null)
            {
                return HttpNotFound();
            }
            return View(rENTAL_COMPANY);
        }

        // GET: RentalCompany/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RentalCompany/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RentalCompanyID,RentalCompanyName,StreetAddress,City,StateAddress,ZIPCode,ContactName,ContactPhone,ContactEmail")] RENTAL_COMPANY rENTAL_COMPANY)
        {
            if (ModelState.IsValid)
            {
                db.RENTAL_COMPANY.Add(rENTAL_COMPANY);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rENTAL_COMPANY);
        }

        // GET: RentalCompany/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RENTAL_COMPANY rENTAL_COMPANY = db.RENTAL_COMPANY.Find(id);
            if (rENTAL_COMPANY == null)
            {
                return HttpNotFound();
            }
            return View(rENTAL_COMPANY);
        }

        // POST: RentalCompany/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RentalCompanyID,RentalCompanyName,StreetAddress,City,StateAddress,ZIPCode,ContactName,ContactPhone,ContactEmail")] RENTAL_COMPANY rENTAL_COMPANY)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rENTAL_COMPANY).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rENTAL_COMPANY);
        }

        // GET: RentalCompany/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RENTAL_COMPANY rENTAL_COMPANY = db.RENTAL_COMPANY.Find(id);
            if (rENTAL_COMPANY == null)
            {
                return HttpNotFound();
            }
            return View(rENTAL_COMPANY);
        }

        // POST: RentalCompany/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RENTAL_COMPANY rENTAL_COMPANY = db.RENTAL_COMPANY.Find(id);
            db.RENTAL_COMPANY.Remove(rENTAL_COMPANY);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
