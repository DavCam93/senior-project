﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ShowStarterSystem2.Models;
using Stripe;

namespace ShowStarterSystem2.Controllers
{

	public class BadgeController : Controller
	{
		private ShowStarterDBEntities db = new ShowStarterDBEntities();

		// GET: Badge
		public ActionResult Index()
		{
			if (User.Identity.IsAuthenticated)
			{
				var badges = db.BADGE.Where(b=>b.USERS.UserName == User.Identity.Name);
				if (User.IsInRole("A"))
				{
					badges = db.BADGE.Where(b => b.BadgeID != null);
				}
				return View(badges);
			}
			else
			{
				return RedirectToAction("Login", "Account");
			}
		}

		// GET: Badge/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			BADGE bADGE = db.BADGE.Find(id);
			if (bADGE == null)
			{
				return HttpNotFound();
			}
			return View(bADGE);
		}

		// GET: Badge/Create
		public ActionResult Create()
		{
			ViewBag.BadgeType = new SelectList(db.BADGE_TYPE, "BadgeTypeID", "BadgeName");
			ViewBag.PurchaserID = new SelectList(db.USERS, "UserID", "UserName");
			return View();
		}

		// POST: Badge/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "BadgeID,PurchaserID,BadgeType,LineOneInfo,LineTwoInfo,hasBeenBought")] BADGE bADGE)
		{
			if (ModelState.IsValid)
			{
				db.BADGE.Add(bADGE);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			ViewBag.BadgeType = new SelectList(db.BADGE_TYPE, "BadgeTypeID", "BadgeName", bADGE.BadgeType);
			ViewBag.PurchaserID = new SelectList(db.USERS, "UserID", "UserName", bADGE.PurchaserID);
			return View(bADGE);
		}

		// GET: Badge/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			BADGE bADGE = db.BADGE.Find(id);
			if (bADGE == null)
			{
				return HttpNotFound();
			}
			ViewBag.BadgeType = new SelectList(db.BADGE_TYPE, "BadgeTypeID", "BadgeName", bADGE.BadgeType);
			ViewBag.PurchaserID = new SelectList(db.USERS, "UserID", "UserName", bADGE.PurchaserID);
			return View(bADGE);
		}

		// POST: Badge/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "BadgeID,PurchaserID,BadgeType,LineOneInfo,LineTwoInfo,hasBeenBought")] BADGE bADGE)
		{
			if (ModelState.IsValid)
			{
				db.Entry(bADGE).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.BadgeType = new SelectList(db.BADGE_TYPE, "BadgeTypeID", "BadgeName", bADGE.BadgeType);
			ViewBag.PurchaserID = new SelectList(db.USERS, "UserID", "UserName", bADGE.PurchaserID);
			return View(bADGE);
		}

		// GET: Badge/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			BADGE bADGE = db.BADGE.Find(id);
			if (bADGE == null)
			{
				return HttpNotFound();
			}
			return View(bADGE);
		}

		// POST: Badge/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			BADGE bADGE = db.BADGE.Find(id);
			db.BADGE.Remove(bADGE);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		public ActionResult Purchase()
		{
			if (!User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Login", "Account");
			}

			var Badges = db.BADGE.Where(b => b.hasBeenBought == false).Where(b => b.USERS.UserName == User.Identity.Name);
			return View(Badges.ToList());
		}

		[HttpPost]
		public ActionResult Purchase(FormContext f)
		{
			String[] keys = Request.Form.AllKeys;
			String[] vals = new String[keys.Length];
			int tokenLoc = 0;
			for (int i = 0; i < keys.Length; i++)
			{
				vals[i] = Request.Form.GetValues(i).FirstOrDefault();
			}

			for (int i = 0; i < keys.Length; i++)
			{
				if(keys[i] == "stripeToken") 
				{
					tokenLoc = i;
					break;
				}
			}


			decimal chargeAmount = 0;
			int numBadges = db.BADGE.Where(b => b.USERS.UserName == User.Identity.Name).Where(b => b.hasBeenBought == false).Count();

			foreach (var item in db.BADGE.Where(b=> b.USERS.UserName == User.Identity.Name).Where(b => b.hasBeenBought == false))
			{
				chargeAmount += (item.BADGE_TYPE.BadgePrice * 100);
			}

			var myCharge = new StripeChargeCreateOptions();

			myCharge.Amount = (int)chargeAmount;
			myCharge.Currency = "usd";
			myCharge.Card = new StripeCreditCardOptions();
			myCharge.Card.TokenId = vals[tokenLoc];

			var chargeService = new StripeChargeService();
			try
			{
				StripeCharge stripeCharge = chargeService.Create(myCharge);
			}
			catch
			{
				return View();
			}

			var b1 = db.BADGE.Where(b => b.USERS.UserName == User.Identity.Name).Where(b => b.hasBeenBought == false).ToArray();
			for (int i = 0; i < b1.Length; i++)
			{
				db.usp_MarkBadgeAsPaid(b1[i].BadgeID);
			}


			return RedirectToAction("Index");
		}


		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
