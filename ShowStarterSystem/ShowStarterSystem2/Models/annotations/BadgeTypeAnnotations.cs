﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ShowStarterSystem2.Models.annotations
{
	[MetadataType(typeof(BadgeTypeMetaData))]
	public partial class BadgeTypeAnnotations
	{
	}

	public class BadgeTypeMetaData
	{

		[DisplayName("Badge Type ID")]
		public int BadgeTypeID { get; set; }

		[DisplayName("Badge Name")]
		public string BadgeName { get; set; }

		[DisplayName("Price (USD)")]
		public decimal BadgePrice { get; set; }

		[DisplayName("Available for Purchase")]
		public bool IsAvailable { get; set; }

		[DisplayName("Badge Days")]
		public int BadgeDays { get; set; }
	}
}