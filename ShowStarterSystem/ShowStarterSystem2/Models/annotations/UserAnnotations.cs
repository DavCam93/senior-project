﻿

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace ShowStarterSystem2.Models
{
	[MetadataType(typeof(UserMetaData))]
	public partial class USER
	{
		public string RawPassword { get; set; }
	}
	public class UserMetaData
	{

		[DisplayName("User Name")]
		public string UserName { get; set; }

		[DisplayName("Legal First Name")]
		public string LegalFirstName { get; set; }

		[DisplayName("Legal Last Name")]
		public string LegalLastName { get; set; }

		[DisplayName("Phone Number")]
		[DataType(DataType.PhoneNumber)]
		public string PhoneNumber { get; set; }

		[DisplayName("Email")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[DisplayName("Street Address")]
		public string StreetAddress { get; set; }

		[DisplayName("State")]
		public string StateAddress { get; set; }

		[DisplayName("ZIP Code")]
		public string ZIPCode { get; set; }

		[DisplayName("Password")]
		[DataType(DataType.Password)]
		public string RawPassword { get; set; }
		
	}
}
