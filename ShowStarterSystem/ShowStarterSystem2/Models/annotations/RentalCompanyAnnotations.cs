﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ShowStarterSystem2.Models
{
	[MetadataType(typeof(RentalCompanyMetaData))]
	public partial class RENTAL_COMPANY
	{
	}

	public class RentalCompanyMetaData
	{

		[DisplayName("Rental Company Name")]
		public string RentalCompanyName { get; set; }

		[DisplayName("Street Address")]
		public string StreetAddress { get; set; }

		[DisplayName("City")]
		public string City { get; set; }

		[DisplayName("State")]
		public string StateAddress { get; set; }

		[DisplayName("ZIP Code")]
		public string ZIPCode { get; set; }

		[DisplayName("Contact Name")]
		public string ContactName { get; set; }

		[DisplayName("Phone Number")]
		[DataType(DataType.PhoneNumber)]
		public string ContactPhone { get; set; }

		[DisplayName("Email Address")]
		[DataType(DataType.EmailAddress)]
		public string ContactEmail { get; set; }
	}
}