﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ShowStarterSystem2.Models
{
	[MetadataType(typeof(BadgeTypeMetaData))]
	public partial class BADGE_TYPE
	{
	}

	public class BadgeTypeMetaData
	{

		[Display(Name = "Badge Name")]
		public string BadgeName;

		[DisplayName("Price")]
		public decimal BadgePrice { get; set; }

		[DisplayName("Is Badge Available")]
		public bool IsAvailable { get; set; }

		[DisplayName("Number of Badge Days")]
		public int BadgeDays { get; set; }
	}

	[MetadataType(typeof(BadgeMetaData))]
	public partial class BADGE
	{

	}

	public class BadgeMetaData
	{
		[Display(Name="Line One Info")]
		public string LineOneInfo;

		[Display(Name = "Line Two Info")]
		public string LineTwoInfo;

		[Display(Name = "Has Been Purchased")]
		public bool hasBeenBought;
	}
}