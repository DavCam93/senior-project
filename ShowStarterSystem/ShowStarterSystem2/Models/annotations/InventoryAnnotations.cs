﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ShowStarterSystem2.Models
{
	[MetadataType(typeof(InventoryMetaData))]
	public partial class INVENTORY_ITEM
	{
	}

	public class InventoryMetaData
	{
		[DisplayName("ID Number")]
		public int InventoryID { get; set; }

		[DisplayName("Short Description")]
		public string ShortDescription { get; set; }

		[DisplayName("Long Description")]
		public string LongDescription { get; set; }

		[DisplayName("Quantity on Hand")]
		public int QuantityOnHand { get; set; }
	}
}