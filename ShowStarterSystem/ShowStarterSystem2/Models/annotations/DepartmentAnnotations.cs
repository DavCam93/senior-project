﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ShowStarterSystem2.Models
{
	[MetadataType(typeof(DepartmentMetaData))]
	public partial class DEPARTMENT
	{
	}

	public class DepartmentMetaData
	{

		[DisplayName("Department ID")]
		public int DepartmentID { get; set; }

		[DisplayName("Name")]
		public string DepartmentName { get; set; }

		[DisplayName("Description")]
		public string DeparmentDescription { get; set; }
	}
}