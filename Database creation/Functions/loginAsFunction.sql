USE [ShowStarterDB]
GO
/****** Object:  StoredProcedure [dbo].[usp_UserLogin]    Script Date: 03/15/2015 23:21:51 ******/
ALTER FUNCTION [dbo].[uf_UserLogin](
	 @UserName	nvarchar(50)
	,@RPassword	nvarchar(max))
	RETURNS @returntable TABLE (userLoggedIn bit)
	

	AS
	begin
	IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName) = 1)
	BEGIN
		
		DECLARE @Salt nchar(25);
		DECLARE @PWwithSalt nvarchar(max);
		DECLARE @PassToCheck varbinary(512);
		SET @Salt = (SELECT Salt FROM USERS WHERE UserName = @UserName);
		SET @PWwithSalt = @RPassword + @Salt;
		SET @PassToCheck = HASHBYTES('SHA2_512',@PWwithSalt)

		IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName AND UserPassword = @PassToCheck) = 1)
		BEGIN
		INSERT INTO @returntable values (1);
		END
		ELSE
		BEGIN
		INSERT INTO @returntable values(0);
		END
	END
	ELSE
	BEGIN
	INSERT INTO @returntable values(0);
	END
	RETURN;
	END
