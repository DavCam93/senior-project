USE [master]
GO
/****** Object:  Database [ShowStarterDB]    Script Date: 04/30/2015 23:27:13 ******/
CREATE DATABASE [ShowStarterDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ShowStarterDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ShowStarterDB.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ShowStarterDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\ShowStarterDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ShowStarterDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ShowStarterDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ShowStarterDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ShowStarterDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ShowStarterDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ShowStarterDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ShowStarterDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [ShowStarterDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ShowStarterDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ShowStarterDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ShowStarterDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ShowStarterDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ShowStarterDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ShowStarterDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ShowStarterDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ShowStarterDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ShowStarterDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ShowStarterDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ShowStarterDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ShowStarterDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ShowStarterDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ShowStarterDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ShowStarterDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ShowStarterDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ShowStarterDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ShowStarterDB] SET  MULTI_USER 
GO
ALTER DATABASE [ShowStarterDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ShowStarterDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ShowStarterDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ShowStarterDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [ShowStarterDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ShowStarterDB]
GO
/****** Object:  UserDefinedFunction [dbo].[uf_UserLogin]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[usp_UserLogin]    Script Date: 03/15/2015 23:21:51 ******/
CREATE FUNCTION [dbo].[uf_UserLogin](
	 @UserName	nvarchar(50)
	,@RPassword	nvarchar(max))
	RETURNS bit
	

	AS
	BEGIN
	IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName) = 1)
	BEGIN
		
		DECLARE @Salt nchar(25);
		DECLARE @PWwithSalt nvarchar(max);
		DECLARE @PassToCheck varbinary(512);
		SET @Salt = (SELECT Salt FROM USERS WHERE UserName = @UserName);
		SET @PWwithSalt = @RPassword + @Salt;
		SET @PassToCheck = HASHBYTES('SHA2_512',@PWwithSalt)

		IF((SELECT UserPassword FROM USERS WHERE UserName = @UserName) = @PassToCheck)
		BEGIN
			RETURN 1;
		END

		ELSE
		BEGIN
			RETURN 0;
		END

	END
	ELSE
	BEGIN
		RETURN 0;
	END
	
	RETURN 0;
	END
GO
/****** Object:  UserDefinedFunction [dbo].[uf_UserLogin2]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[usp_UserLogin]    Script Date: 03/15/2015 23:21:51 ******/
CREATE FUNCTION [dbo].[uf_UserLogin2](
	 @UserName	nvarchar(50)
	,@RPassword	nvarchar(max))
	RETURNS @returntable TABLE (userLoggedIn bit)
	

	AS
	begin
	IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName) = 1)
	BEGIN
		
		DECLARE @Salt nchar(25);
		DECLARE @PWwithSalt nvarchar(max);
		DECLARE @PassToCheck varbinary(512);
		SET @Salt = (SELECT Salt FROM USERS WHERE UserName = @UserName);
		SET @PWwithSalt = @RPassword + @Salt;
		SET @PassToCheck = HASHBYTES('SHA2_512',@PWwithSalt)

		IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName AND UserPassword = @PassToCheck) = 1)
		BEGIN
		INSERT INTO @returntable values (1);
		END
		ELSE
		BEGIN
		INSERT INTO @returntable values(0);
		END
	END
	ELSE
	BEGIN
	INSERT INTO @returntable values(0);
	END
	RETURN;
	END

GO
/****** Object:  Table [dbo].[BADGE]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BADGE](
	[BadgeID] [int] IDENTITY(1,1) NOT NULL,
	[PurchaserID] [int] NOT NULL,
	[BadgeType] [int] NOT NULL,
	[LineOneInfo] [nvarchar](50) NOT NULL,
	[LineTwoInfo] [nvarchar](50) NULL,
	[hasBeenBought] [bit] NOT NULL CONSTRAINT [DF_BADGE_hasBeenBought]  DEFAULT ((0)),
 CONSTRAINT [PK_Badge] PRIMARY KEY CLUSTERED 
(
	[BadgeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[BADGE_TYPE]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BADGE_TYPE](
	[BadgeTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ShowID] [int] NOT NULL,
	[BadgeName] [nvarchar](50) NOT NULL,
	[BadgePrice] [smallmoney] NOT NULL,
	[IsAvailable] [bit] NOT NULL DEFAULT ((0)),
	[BadgeDays] [int] NOT NULL CONSTRAINT [DF_BADGE_TYPE_BadgeDays]  DEFAULT ((0)),
 CONSTRAINT [PK_BadgeType] PRIMARY KEY CLUSTERED 
(
	[BadgeTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DEPARTMENT]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DEPARTMENT](
	[DepartmentID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentName] [nvarchar](255) NOT NULL,
	[DeparmentDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_DeptID] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVENT_PANELISTS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_PANELISTS](
	[EventID] [int] NOT NULL,
	[PanelistID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[EVENT_TRACKS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_TRACKS](
	[EventID] [int] NOT NULL,
	[TrackID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GROUPS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GROUPS](
	[GroupID] [nvarchar](5) NOT NULL,
	[GroupDescription] [nvarchar](255) NULL,
 CONSTRAINT [PK_Group] PRIMARY KEY CLUSTERED 
(
	[GroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[INVENTORY_ITEM]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[INVENTORY_ITEM](
	[InventoryID] [int] IDENTITY(1,1) NOT NULL,
	[ShortDescription] [nvarchar](255) NOT NULL,
	[LongDescription] [nvarchar](max) NULL,
	[QuantityOnHand] [int] NOT NULL,
	[Department] [int] NOT NULL,
 CONSTRAINT [PK_InventoryID] PRIMARY KEY CLUSTERED 
(
	[InventoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RENTAL_COMPANY]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RENTAL_COMPANY](
	[RentalCompanyID] [int] IDENTITY(1,1) NOT NULL,
	[RentalCompanyName] [nvarchar](255) NOT NULL,
	[StreetAddress] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[StateAddress] [nchar](2) NULL,
	[ZIPCode] [nchar](5) NULL,
	[ContactName] [nvarchar](255) NULL,
	[ContactPhone] [nchar](13) NULL,
	[ContactEmail] [nvarchar](255) NULL,
 CONSTRAINT [PK_RentalCompanyID] PRIMARY KEY CLUSTERED 
(
	[RentalCompanyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RENTAL_ITEMS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RENTAL_ITEMS](
	[InventoryID] [int] NOT NULL,
	[RentalCompanyID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ROOM]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROOM](
	[RoomID] [int] IDENTITY(1,1) NOT NULL,
	[VenueID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsSinglePurpose] [bit] NOT NULL,
	[MainPurpose] [nvarchar](max) NULL,
	[IsCombinedRoom] [bit] NOT NULL,
 CONSTRAINT [PK_RoomID] PRIMARY KEY CLUSTERED 
(
	[RoomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ROOM_CONFIGURATION]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ROOM_CONFIGURATION](
	[ConfigurationID] [int] IDENTITY(1,1) NOT NULL,
	[RoomID] [int] NOT NULL,
	[ConfigurationName] [nvarchar](50) NOT NULL,
	[ConfigDescription] [nvarchar](max) NULL,
	[Capacity] [int] NOT NULL,
	[EstimatedSwitchTime] [time](7) NOT NULL,
 CONSTRAINT [PK_ConfigurationID] PRIMARY KEY CLUSTERED 
(
	[ConfigurationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SHOW]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SHOW](
	[ShowID] [int] IDENTITY(1,1) NOT NULL,
	[ShowName] [nvarchar](255) NOT NULL,
	[StartDate] [datetime2](0) NULL,
	[EndDate] [datetime2](0) NULL,
	[Theme] [nvarchar](255) NULL,
	[isDefaultShow] [bit] NOT NULL DEFAULT ('0'),
 CONSTRAINT [PK_ShowID] PRIMARY KEY CLUSTERED 
(
	[ShowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SHOW_DEPARTMENTS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SHOW_DEPARTMENTS](
	[ShowID] [int] NOT NULL,
	[DepartmentID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SHOW_EVENT]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SHOW_EVENT](
	[EventID] [int] IDENTITY(1,1) NOT NULL,
	[EventName] [nvarchar](255) NOT NULL,
	[EventDescription] [nvarchar](max) NULL,
	[RequestedEquipment] [nvarchar](max) NULL,
	[RoomConfiguration] [int] NULL,
	[ShowID] [int] NOT NULL,
	[StartTime] [datetime2](0) NULL,
	[EndTime] [datetime2](0) NULL,
	[ExpectedAttendance] [int] NOT NULL,
	[ActualAttendance] [int] NULL,
	[WasInPreviousShow] [bit] NOT NULL,
 CONSTRAINT [PK_EventID] PRIMARY KEY CLUSTERED 
(
	[EventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SHOW_VENUES]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SHOW_VENUES](
	[ShowID] [int] NOT NULL,
	[VenueID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[STAFF_POSITION]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STAFF_POSITION](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[PositionName] [nvarchar](50) NOT NULL,
	[PositionDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_PositionID] PRIMARY KEY CLUSTERED 
(
	[PositionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TEXT_ALERT]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TEXT_ALERT](
	[TextAlertID] [int] IDENTITY(1,1) NOT NULL,
	[AlertMessage] [nvarchar](160) NOT NULL,
	[ScheduledSendTime] [datetime2](0) NOT NULL,
	[SentTime] [datetime2](0) NULL,
 CONSTRAINT [PK_TextAlert] PRIMARY KEY CLUSTERED 
(
	[TextAlertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TRACK]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TRACK](
	[TrackID] [int] IDENTITY(1,1) NOT NULL,
	[TrackName] [nvarchar](255) NOT NULL,
	[TrackDescription] [nvarchar](max) NULL,
 CONSTRAINT [PK_Track] PRIMARY KEY CLUSTERED 
(
	[TrackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USER_ALERTS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_ALERTS](
	[AlertID] [int] NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USER_GROUPS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_GROUPS](
	[GroupID] [nvarchar](5) NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USER_POSITIONS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_POSITIONS](
	[PositionID] [int] NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USER_SCHEDULE]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[USER_SCHEDULE](
	[EventID] [int] NOT NULL,
	[UserID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[USERS]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[USERS](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[UserPassword] [varbinary](512) NOT NULL,
	[Salt] [nchar](25) NOT NULL,
	[LegalFirstName] [nvarchar](50) NOT NULL,
	[LegalLastName] [nvarchar](50) NOT NULL,
	[PhoneNumber] [nchar](13) NULL,
	[Email] [nvarchar](255) NOT NULL,
	[StreetAddress] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[StateAddress] [nchar](2) NULL,
	[ZIPCode] [nchar](5) NULL,
 CONSTRAINT [PK_UserID] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_UserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VENUE]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VENUE](
	[VenueID] [int] IDENTITY(1,1) NOT NULL,
	[VenueName] [nvarchar](255) NOT NULL,
	[StreetAddress] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[StateAddress] [nchar](2) NULL,
	[ZIPCode] [nchar](5) NULL,
	[ContactName] [nvarchar](255) NULL,
	[ContactPhone] [nchar](13) NULL,
	[ContactEmail] [nvarchar](255) NULL,
 CONSTRAINT [PK_VenueID] PRIMARY KEY CLUSTERED 
(
	[VenueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[INVENTORY_ITEM] ADD  DEFAULT ((1)) FOR [QuantityOnHand]
GO
ALTER TABLE [dbo].[ROOM] ADD  DEFAULT ((0)) FOR [IsSinglePurpose]
GO
ALTER TABLE [dbo].[ROOM] ADD  DEFAULT ((0)) FOR [IsCombinedRoom]
GO
ALTER TABLE [dbo].[ROOM_CONFIGURATION] ADD  DEFAULT ((0)) FOR [Capacity]
GO
ALTER TABLE [dbo].[SHOW_EVENT] ADD  DEFAULT ((0)) FOR [ExpectedAttendance]
GO
ALTER TABLE [dbo].[SHOW_EVENT] ADD  DEFAULT ((0)) FOR [WasInPreviousShow]
GO
ALTER TABLE [dbo].[BADGE]  WITH CHECK ADD  CONSTRAINT [FK_BADGE_BadgeType] FOREIGN KEY([BadgeType])
REFERENCES [dbo].[BADGE_TYPE] ([BadgeTypeID])
GO
ALTER TABLE [dbo].[BADGE] CHECK CONSTRAINT [FK_BADGE_BadgeType]
GO
ALTER TABLE [dbo].[BADGE]  WITH CHECK ADD  CONSTRAINT [FK_BADGE_Purchaser] FOREIGN KEY([PurchaserID])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[BADGE] CHECK CONSTRAINT [FK_BADGE_Purchaser]
GO
ALTER TABLE [dbo].[BADGE_TYPE]  WITH CHECK ADD  CONSTRAINT [FK_BT_ShowID] FOREIGN KEY([ShowID])
REFERENCES [dbo].[SHOW] ([ShowID])
GO
ALTER TABLE [dbo].[BADGE_TYPE] CHECK CONSTRAINT [FK_BT_ShowID]
GO
ALTER TABLE [dbo].[EVENT_PANELISTS]  WITH CHECK ADD  CONSTRAINT [FK_EP_EventID] FOREIGN KEY([EventID])
REFERENCES [dbo].[SHOW_EVENT] ([EventID])
GO
ALTER TABLE [dbo].[EVENT_PANELISTS] CHECK CONSTRAINT [FK_EP_EventID]
GO
ALTER TABLE [dbo].[EVENT_PANELISTS]  WITH CHECK ADD  CONSTRAINT [FK_EP_PanelistID] FOREIGN KEY([PanelistID])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[EVENT_PANELISTS] CHECK CONSTRAINT [FK_EP_PanelistID]
GO
ALTER TABLE [dbo].[EVENT_TRACKS]  WITH CHECK ADD  CONSTRAINT [FK_ET_EventID] FOREIGN KEY([EventID])
REFERENCES [dbo].[SHOW_EVENT] ([EventID])
GO
ALTER TABLE [dbo].[EVENT_TRACKS] CHECK CONSTRAINT [FK_ET_EventID]
GO
ALTER TABLE [dbo].[EVENT_TRACKS]  WITH CHECK ADD  CONSTRAINT [FK_ET_TrackID] FOREIGN KEY([TrackID])
REFERENCES [dbo].[TRACK] ([TrackID])
GO
ALTER TABLE [dbo].[EVENT_TRACKS] CHECK CONSTRAINT [FK_ET_TrackID]
GO
ALTER TABLE [dbo].[INVENTORY_ITEM]  WITH CHECK ADD  CONSTRAINT [FK_II_Department] FOREIGN KEY([Department])
REFERENCES [dbo].[DEPARTMENT] ([DepartmentID])
GO
ALTER TABLE [dbo].[INVENTORY_ITEM] CHECK CONSTRAINT [FK_II_Department]
GO
ALTER TABLE [dbo].[RENTAL_ITEMS]  WITH CHECK ADD  CONSTRAINT [FK_RI_InventoryID] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[INVENTORY_ITEM] ([InventoryID])
GO
ALTER TABLE [dbo].[RENTAL_ITEMS] CHECK CONSTRAINT [FK_RI_InventoryID]
GO
ALTER TABLE [dbo].[RENTAL_ITEMS]  WITH CHECK ADD  CONSTRAINT [FK_RI_RentalCompanyID] FOREIGN KEY([RentalCompanyID])
REFERENCES [dbo].[RENTAL_COMPANY] ([RentalCompanyID])
GO
ALTER TABLE [dbo].[RENTAL_ITEMS] CHECK CONSTRAINT [FK_RI_RentalCompanyID]
GO
ALTER TABLE [dbo].[ROOM]  WITH CHECK ADD  CONSTRAINT [FK_Room_Venue] FOREIGN KEY([VenueID])
REFERENCES [dbo].[VENUE] ([VenueID])
GO
ALTER TABLE [dbo].[ROOM] CHECK CONSTRAINT [FK_Room_Venue]
GO
ALTER TABLE [dbo].[ROOM_CONFIGURATION]  WITH CHECK ADD  CONSTRAINT [FK_RConf_RoomID] FOREIGN KEY([RoomID])
REFERENCES [dbo].[ROOM] ([RoomID])
GO
ALTER TABLE [dbo].[ROOM_CONFIGURATION] CHECK CONSTRAINT [FK_RConf_RoomID]
GO
ALTER TABLE [dbo].[SHOW_DEPARTMENTS]  WITH CHECK ADD  CONSTRAINT [FK_SD_DepartmentID] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[DEPARTMENT] ([DepartmentID])
GO
ALTER TABLE [dbo].[SHOW_DEPARTMENTS] CHECK CONSTRAINT [FK_SD_DepartmentID]
GO
ALTER TABLE [dbo].[SHOW_DEPARTMENTS]  WITH CHECK ADD  CONSTRAINT [FK_SD_ShowID] FOREIGN KEY([ShowID])
REFERENCES [dbo].[SHOW] ([ShowID])
GO
ALTER TABLE [dbo].[SHOW_DEPARTMENTS] CHECK CONSTRAINT [FK_SD_ShowID]
GO
ALTER TABLE [dbo].[SHOW_EVENT]  WITH CHECK ADD  CONSTRAINT [FK_Event_RoomConfig] FOREIGN KEY([RoomConfiguration])
REFERENCES [dbo].[ROOM_CONFIGURATION] ([ConfigurationID])
GO
ALTER TABLE [dbo].[SHOW_EVENT] CHECK CONSTRAINT [FK_Event_RoomConfig]
GO
ALTER TABLE [dbo].[SHOW_EVENT]  WITH CHECK ADD  CONSTRAINT [FK_Event_Show] FOREIGN KEY([ShowID])
REFERENCES [dbo].[SHOW] ([ShowID])
GO
ALTER TABLE [dbo].[SHOW_EVENT] CHECK CONSTRAINT [FK_Event_Show]
GO
ALTER TABLE [dbo].[SHOW_VENUES]  WITH CHECK ADD  CONSTRAINT [FK_SV_ShowID] FOREIGN KEY([ShowID])
REFERENCES [dbo].[SHOW] ([ShowID])
GO
ALTER TABLE [dbo].[SHOW_VENUES] CHECK CONSTRAINT [FK_SV_ShowID]
GO
ALTER TABLE [dbo].[SHOW_VENUES]  WITH CHECK ADD  CONSTRAINT [FK_SV_VenueID] FOREIGN KEY([VenueID])
REFERENCES [dbo].[VENUE] ([VenueID])
GO
ALTER TABLE [dbo].[SHOW_VENUES] CHECK CONSTRAINT [FK_SV_VenueID]
GO
ALTER TABLE [dbo].[STAFF_POSITION]  WITH CHECK ADD  CONSTRAINT [FK_SP_DepartmentID] FOREIGN KEY([DepartmentID])
REFERENCES [dbo].[DEPARTMENT] ([DepartmentID])
GO
ALTER TABLE [dbo].[STAFF_POSITION] CHECK CONSTRAINT [FK_SP_DepartmentID]
GO
ALTER TABLE [dbo].[USER_ALERTS]  WITH CHECK ADD  CONSTRAINT [FK_UA_AlertID] FOREIGN KEY([AlertID])
REFERENCES [dbo].[TEXT_ALERT] ([TextAlertID])
GO
ALTER TABLE [dbo].[USER_ALERTS] CHECK CONSTRAINT [FK_UA_AlertID]
GO
ALTER TABLE [dbo].[USER_ALERTS]  WITH CHECK ADD  CONSTRAINT [FK_UA_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[USER_ALERTS] CHECK CONSTRAINT [FK_UA_UserID]
GO
ALTER TABLE [dbo].[USER_GROUPS]  WITH CHECK ADD  CONSTRAINT [FK_UG_GroupID] FOREIGN KEY([GroupID])
REFERENCES [dbo].[GROUPS] ([GroupID])
GO
ALTER TABLE [dbo].[USER_GROUPS] CHECK CONSTRAINT [FK_UG_GroupID]
GO
ALTER TABLE [dbo].[USER_GROUPS]  WITH CHECK ADD  CONSTRAINT [FK_UG_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[USER_GROUPS] CHECK CONSTRAINT [FK_UG_UserID]
GO
ALTER TABLE [dbo].[USER_POSITIONS]  WITH CHECK ADD  CONSTRAINT [FK_UP_PositionID] FOREIGN KEY([PositionID])
REFERENCES [dbo].[STAFF_POSITION] ([PositionID])
GO
ALTER TABLE [dbo].[USER_POSITIONS] CHECK CONSTRAINT [FK_UP_PositionID]
GO
ALTER TABLE [dbo].[USER_POSITIONS]  WITH CHECK ADD  CONSTRAINT [FK_UP_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[USER_POSITIONS] CHECK CONSTRAINT [FK_UP_UserID]
GO
ALTER TABLE [dbo].[USER_SCHEDULE]  WITH CHECK ADD  CONSTRAINT [FK_US_EventID] FOREIGN KEY([EventID])
REFERENCES [dbo].[SHOW_EVENT] ([EventID])
GO
ALTER TABLE [dbo].[USER_SCHEDULE] CHECK CONSTRAINT [FK_US_EventID]
GO
ALTER TABLE [dbo].[USER_SCHEDULE]  WITH CHECK ADD  CONSTRAINT [FK_US_UserID] FOREIGN KEY([UserID])
REFERENCES [dbo].[USERS] ([UserID])
GO
ALTER TABLE [dbo].[USER_SCHEDULE] CHECK CONSTRAINT [FK_US_UserID]
GO
ALTER TABLE [dbo].[RENTAL_COMPANY]  WITH CHECK ADD  CONSTRAINT [CK_RentalCompanyPhone] CHECK  (([ContactPhone] like '([1-9][1-9][1-9])[1-9][1-9][1-9]-[1-9][1-9][1-9][1-9]'))
GO
ALTER TABLE [dbo].[RENTAL_COMPANY] CHECK CONSTRAINT [CK_RentalCompanyPhone]
GO
ALTER TABLE [dbo].[RENTAL_COMPANY]  WITH CHECK ADD  CONSTRAINT [CK_RentalCompanyState] CHECK  (([StateAddress] like '[A-Z][A-Z]'))
GO
ALTER TABLE [dbo].[RENTAL_COMPANY] CHECK CONSTRAINT [CK_RentalCompanyState]
GO
ALTER TABLE [dbo].[RENTAL_COMPANY]  WITH CHECK ADD  CONSTRAINT [CK_RentalCompanyZIP] CHECK  (([ZIPCode] like '[1-9][1-9][1-9][1-9][1-9]'))
GO
ALTER TABLE [dbo].[RENTAL_COMPANY] CHECK CONSTRAINT [CK_RentalCompanyZIP]
GO
ALTER TABLE [dbo].[USERS]  WITH CHECK ADD  CONSTRAINT [CK_UserPhone] CHECK  (([PhoneNumber] like '([1-9][1-9][1-9])[1-9][1-9][1-9]-[1-9][1-9][1-9][1-9]'))
GO
ALTER TABLE [dbo].[USERS] CHECK CONSTRAINT [CK_UserPhone]
GO
ALTER TABLE [dbo].[USERS]  WITH CHECK ADD  CONSTRAINT [CK_UserState] CHECK  (([StateAddress] like '[A-Z][A-Z]'))
GO
ALTER TABLE [dbo].[USERS] CHECK CONSTRAINT [CK_UserState]
GO
ALTER TABLE [dbo].[USERS]  WITH CHECK ADD  CONSTRAINT [CK_UserZIP] CHECK  (([ZIPCode] like '[1-9][1-9][1-9][1-9][1-9]'))
GO
ALTER TABLE [dbo].[USERS] CHECK CONSTRAINT [CK_UserZIP]
GO
ALTER TABLE [dbo].[VENUE]  WITH CHECK ADD  CONSTRAINT [CK_VenueState] CHECK  (([StateAddress] like '[A-Z][A-Z]'))
GO
ALTER TABLE [dbo].[VENUE] CHECK CONSTRAINT [CK_VenueState]
GO
ALTER TABLE [dbo].[VENUE]  WITH CHECK ADD  CONSTRAINT [CK_VenueZIP] CHECK  (([ZIPCode] like '[1-9][1-9][1-9][1-9][1-9]'))
GO
ALTER TABLE [dbo].[VENUE] CHECK CONSTRAINT [CK_VenueZIP]
GO
/****** Object:  StoredProcedure [dbo].[usp_AddEvent]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddEvent]
	@Name			nvarchar(255)
	,@Description	nvarchar(max) = null
	,@REquipment	nvarchar(max) = null
	,@ShowID		int
	,@ExpectedAttendance	int
	,@WasInPreviousShow		bit
	--,@EventLength			time
AS
--TODO FIGURE OUT TIME LENGTH REQUIREMENTS

INSERT INTO SHOW_EVENT(EventName,EventDescription,RequestedEquipment,ShowID,ExpectedAttendance,WasInPreviousShow)
VALUES (@Name,@Description,@REquipment,@ShowID,@ExpectedAttendance,@WasInPreviousShow)
GO
/****** Object:  StoredProcedure [dbo].[usp_AddGroup]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddGroup]
	@GroupID nvarchar(5)
	,@GroupDescription	nvarchar(max) = null

	AS
	
	INSERT INTO GROUPS(GroupID, GroupDescription)
	VALUES(@GroupID, @GroupDescription)
GO
/****** Object:  StoredProcedure [dbo].[usp_AddUser]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddUser]
	 @UserName		nvarchar(50)
	,@UserEmail		nvarchar(255)
	,@LFirstName	nvarchar(50)
	,@LLastName		nvarchar(50)
	,@RawPassword	nvarchar(MAX)

AS

--Salting function thatnks to: http://www.mssqltips.com/sqlservertip/3293/add-a-salt-with-the-sql-server-hashbytes-function/
DECLARE @Salt nvarchar(25);
DECLARE @PWWithSalt nvarchar(280);
DECLARE @Seed int;
DECLARE @LCV tinyint;
DECLARE @CTime DateTime;

SET @CTime = GETDATE();
SET @Seed = (DATEPART(hh, @Ctime) * 10000000) + (DATEPART(n, @CTime) * 100000) 
      + (DATEPART(s, @CTime) * 1000) + DATEPART(ms, @CTime);
SET @LCV = 1;

SET @Salt = CHAR(ROUND((RAND(@Seed) * 76.0) + 65, 3));
--add characters to the salt until we hit the 25 we need
WHILE (@LCV < 25)
BEGIN
	SET @Salt = @Salt + CHAR(ROUND((RAND() * 76.0) + 64, 3));
	SET @LCV = @LCV + 1;
END;

SET @PWWithSalt = @RawPassword + @Salt;

INSERT INTO USERS (UserName,LegalFirstName,LegalLastName,Email,Salt,UserPassword)
VALUES(@UserName, @LFirstName, @LLastName,@UserEmail,@Salt,HASHBYTES('SHA2_512',@PWWithSalt));

IF((Select COUNT(1) from USERS) = 1) 
BEGIN
	DECLARE @GroupID nvarchar(5);
	SET @GroupID = N'A'
	EXEC usp_AddGroup @GroupID = N'A',@GroupDescription = 'Administrators';

	EXEC usp_AddUserToGroup @GroupID = N'A', @UserID = 1;

END
GO
/****** Object:  StoredProcedure [dbo].[usp_AddUserToGroup]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddUserToGroup]
	 @GroupID nvarchar(5)
	,@UserID int

	AS

	INSERT INTO USER_GROUPS(GroupID,UserID) VALUES (@GroupID,@UserID)
GO
/****** Object:  StoredProcedure [dbo].[usp_AddVenue]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AddVenue]
		 @Name			nvarchar(255)
		,@StreetAddress	nvarchar(255)	=	null
		,@city			nvarchar(255)	=	null
		,@venuestate	nchar(2)		=	null
		,@ZIPCode		nchar(5)		=	null
		,@ContactName	nvarchar(255)	=	null
		,@ContactPhone	nchar(13)		=	null
		,@ContactEmail	nchar(255)	=	null
AS

INSERT INTO VENUE(VenueName,StreetAddress,City,StateAddress,ZIPCode,ContactName,ContactPhone,ContactEmail)
VALUES(@Name,@StreetAddress,@city,@venuestate,@ZIPCode,@ContactName,@ContactPhone,@ContactEmail)
GO
/****** Object:  StoredProcedure [dbo].[usp_CreateNewShow]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CreateNewShow]
	 @ShowName		nvarchar(255)
	,@StartDate		datetime2(0)		= NULL
	,@EndDate		datetime2(0)		= NULL
	,@Theme			nvarchar(255)		= NULL

AS
	INSERT INTO SHOW(ShowName, StartDate, EndDate, Theme)
	VALUES(@ShowName, @StartDate, @EndDate, @Theme)
	

GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteEvent]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_DeleteEvent]
		@EventID int
AS
	DELETE FROM USER_SCHEDULE WHERE EventID = @EventID;
	DELETE FROM EVENT_PANELISTS WHERE EventID = @EventID;
	DELETE FROM SHOW_EVENT WHERE EventID = @EventID
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteShow]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteShow]
	@ShowID int
AS DELETE FROM SHOW WHERE ShowID = @ShowID
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteVenue]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_DeleteVenue]
	@VenueID int
AS

DELETE FROM VENUE WHERE @VenueID = VenueID;
DELETE FROM SHOW_VENUES WHERE @VenueID = VenueID;
DELETE ROOM_CONFIGURATION 
FROM ROOM_CONFIGURATION JOIN ROOM ON ROOM.RoomID = ROOM_CONFIGURATION.RoomID
WHERE VenueID = @VenueID;
DELETE FROM ROOM WHERE @VenueID = VenueID;

GO
/****** Object:  StoredProcedure [dbo].[usp_EditEvent]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EditEvent]
	@EventID			INT
	,@EventName			nvarchar(255)
	,@EventDescription	nvarchar(max)
	,@REquipment		nvarchar(max)
	,@RoomConfig		INT
	,@ShowID			int
	,@StartTime			datetime2(0)
	,@EndTime			datetime2(0)
	,@ExpectedAttendance	int
	,@ActualAttendance		int
	,@WasInPreviousShow		bit

AS

	UPDATE SHOW_EVENT
	SET
	EventName = @EventName
	,EventDescription = @EventDescription
	,RequestedEquipment = @REquipment
	,RoomConfiguration = @RoomConfig
	,ShowID = @ShowID
	,StartTime = @StartTime
	,EndTime = @EndTime
	,ExpectedAttendance = @ExpectedAttendance
	,ActualAttendance = @ActualAttendance
	,WasInPreviousShow = @WasInPreviousShow
	WHERE EventID = @EventID
GO
/****** Object:  StoredProcedure [dbo].[usp_EditShowDetails]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_EditShowDetails]
		 @ShowID		Int
		,@ShowName		nvarchar(255)	= NULL
		,@StartDate		datetime2(0)	= NULL
		,@EndDate		datetime2(0)	= NULL
		,@Theme			nvarchar(255)   = NULL
		,@isDefault		bit				= '0'
AS
	UPDATE SHOW
	 SET ShowName = @ShowName
	,StartDate = @StartDate
	,EndDate = @EndDate
	,Theme = @Theme
	,isDefaultShow = @isDefault
	WHERE ShowID = @ShowID
GO
/****** Object:  StoredProcedure [dbo].[usp_EditVenue]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EditVenue]
		 @Name			nvarchar(255)
		,@StreetAddress	nvarchar(255)	=	null
		,@city			nvarchar(255)	=	null
		,@venuestate	nchar(2)		=	null
		,@ZIPCode		nchar(5)		=	null
		,@ContactName	nvarchar(255)	=	null
		,@ContactPhone	nchar(13)		=	null
		,@ContactEmail	nchar(255)	=	null
		,@VenueID		int
AS

UPDATE VENUE
SET VenueName = @Name
	,StreetAddress = @StreetAddress
	,City = @city
	,StateAddress = @venuestate
	,ZIPCode = @ZIPCode
	,ContactName = @ContactName
	,ContactPhone = @ContactPhone
	,ContactEmail = @ContactEmail
Where VenueID = @VenueID
GO
/****** Object:  StoredProcedure [dbo].[usp_MarkBadgeAsPaid]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MarkBadgeAsPaid]
	@badgeID int 
	
	AS

	UPDATE BADGE
	SET hasBeenBought = '1'
	WHERE BadgeID = @BadgeID

GO
/****** Object:  StoredProcedure [dbo].[usp_UserLogin]    Script Date: 04/30/2015 23:27:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_UserLogin]
	 @UserName	nvarchar(50)
	,@RPassword	nvarchar(max)
	,@UserPassCorrect bit = NULL OUTPUT

	AS

	IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName) = 1)
	BEGIN
		
		DECLARE @Salt nchar(25);
		DECLARE @PWwithSalt nvarchar(max);
		DECLARE @PassToCheck varbinary(512);
		SET @Salt = (SELECT Salt FROM USERS WHERE UserName = @UserName);
		SET @PWwithSalt = @RPassword + @Salt;
		SET @PassToCheck = HASHBYTES('SHA2_512',@PWwithSalt)

		IF((SELECT UserPassword FROM USERS WHERE UserName = @UserName) = @PassToCheck)
		BEGIN
			SET @UserPassCorrect = 1;
			RETURN;
		END

		ELSE
		BEGIN
			SET @UserPassCorrect = 0;
			RETURN;
		END

	END
	ELSE
	BEGIN
		SET @UserPassCorrect = 0;
		RETURN;
	END
GO
USE [master]
GO
ALTER DATABASE [ShowStarterDB] SET  READ_WRITE 
GO
