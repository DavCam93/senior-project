USE [ShowStarterDB]
GO
/****** Object:  StoredProcedure [dbo].[usp_AddUser]    Script Date: 03/10/2015 01:34:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_AddUser]
	 @UserName		nvarchar(50)
	,@UserEmail		nvarchar(255)
	,@LFirstName	nvarchar(50)
	,@LLastName		nvarchar(50)
	,@RawPassword	nvarchar(MAX)

AS

--Salting function thatnks to: http://www.mssqltips.com/sqlservertip/3293/add-a-salt-with-the-sql-server-hashbytes-function/
DECLARE @Salt nvarchar(25);
DECLARE @PWWithSalt nvarchar(280);
DECLARE @Seed int;
DECLARE @LCV tinyint;
DECLARE @CTime DateTime;

SET @CTime = GETDATE();
SET @Seed = (DATEPART(hh, @Ctime) * 10000000) + (DATEPART(n, @CTime) * 100000) 
      + (DATEPART(s, @CTime) * 1000) + DATEPART(ms, @CTime);
SET @LCV = 1;

SET @Salt = CHAR(ROUND((RAND(@Seed) * 76.0) + 65, 3));
--add characters to the salt until we hit the 25 we need
WHILE (@LCV < 25)
BEGIN
	SET @Salt = @Salt + CHAR(ROUND((RAND() * 76.0) + 64, 3));
	SET @LCV = @LCV + 1;
END;

SET @PWWithSalt = @RawPassword + @Salt;

INSERT INTO USERS (UserName,LegalFirstName,LegalLastName,Email,Salt,UserPassword)
VALUES(@UserName, @LFirstName, @LLastName,@UserEmail,@Salt,HASHBYTES('SHA2_512',@PWWithSalt));

IF((Select COUNT(1) from USERS) = 1) 
BEGIN
	DECLARE @GroupID nvarchar(5);
	SET @GroupID = N'A'
	EXEC usp_AddGroup @GroupID = N'A',@GroupDescription = 'Administrators';

	EXEC usp_AddUserToGroup @GroupID = N'A', @UserID = 1;

END