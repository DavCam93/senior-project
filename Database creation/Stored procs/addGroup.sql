CREATE PROCEDURE usp_AddGroup
	@GroupID nvarchar(5)
	,@GroupDescription	nvarchar(max) = null

	AS
	
	INSERT INTO GROUPS(GroupID, GroupDescription)
	VALUES(@GroupID, @GroupDescription)