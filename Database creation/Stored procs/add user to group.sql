CREATE PROCEDURE usp_AddUserToGroup
	 @GroupID nvarchar(5)
	,@UserID int

	AS

	INSERT INTO USER_GROUPS(GroupID,UserID) VALUES (@GroupID,@UserID)