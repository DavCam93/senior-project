ALTER PROCEDURE usp_UserLogin
	 @UserName	nvarchar(50)
	,@RPassword	nvarchar(max)
	,@UserPassCorrect bit = NULL OUTPUT

	AS

	IF((SELECT COUNT(1) FROM USERS WHERE UserName  = @UserName) = 1)
	BEGIN
		
		DECLARE @Salt nchar(25);
		DECLARE @PWwithSalt nvarchar(max);
		DECLARE @PassToCheck varbinary(512);
		SET @Salt = (SELECT Salt FROM USERS WHERE UserName = @UserName);
		SET @PWwithSalt = @RPassword + @Salt;
		SET @PassToCheck = HASHBYTES('SHA2_512',@PWwithSalt)

		IF((SELECT UserPassword FROM USERS WHERE UserName = @UserName) = @PassToCheck)
		BEGIN
			SET @UserPassCorrect = 1;
			RETURN;
		END

		ELSE
		BEGIN
			SET @UserPassCorrect = 0;
			RETURN;
		END

	END
	ELSE
	BEGIN
		SET @UserPassCorrect = 0;
		RETURN;
	END