CREATE PROCEDURE usp_AddEvent
	@Name			nvarchar(255)
	,@Description	nvarchar(max) = null
	,@REquipment	nvarchar(max) = null
	,@ShowID		int
	,@ExpectedAttendance	int
	,@WasInPreviousShow		bit
	--,@EventLength			time
AS
--TODO FIGURE OUT TIME LENGTH REQUIREMENTS

INSERT INTO SHOW_EVENT(EventName,EventDescription,RequestedEquipment,ShowID,ExpectedAttendance,WasInPreviousShow)
VALUES (@Name,@Description,@REquipment,@ShowID,@ExpectedAttendance,@WasInPreviousShow)