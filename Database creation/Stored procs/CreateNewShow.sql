CREATE PROCEDURE dbo.usp_CreateNewShow
	 @ShowName		nvarchar(255)
	,@StartDate		datetime2(0)		= NULL
	,@EndDate		datetime2(0)		= NULL
	,@Theme			nvarchar(255)		= NULL

AS
	INSERT INTO SHOW(ShowName, StartDate, EndDate, Theme)
	VALUES(@ShowName, @StartDate, @EndDate, @Theme)
	
GO