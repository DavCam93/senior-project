
CREATE PROCEDURE usp_EditShowDetails
		 @ShowID		Int
		,@ShowName		nvarchar(255)	= NULL
		,@StartDate		datetime2(0)	= NULL
		,@EndDate		datetime2(0)	= NULL
		,@Theme			nvarchar(255)   = NULL
		,@isDefault		bit				= '0'
AS
	UPDATE SHOW
	 SET ShowName = @ShowName
	,StartDate = @StartDate
	,EndDate = @EndDate
	,Theme = @Theme
	,isDefaultShow = @isDefault
	WHERE ShowID = @ShowID