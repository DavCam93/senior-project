CREATE PROCEDURE usp_EditVenue
		 @Name			nvarchar(255)
		,@StreetAddress	nvarchar(255)	=	null
		,@city			nvarchar(255)	=	null
		,@venuestate	nchar(2)		=	null
		,@ZIPCode		nchar(5)		=	null
		,@ContactName	nvarchar(255)	=	null
		,@ContactPhone	nchar(13)		=	null
		,@ContactEmail	nchar(255)	=	null
		,@VenueID		int
AS

UPDATE VENUE
SET VenueName = @Name
	,StreetAddress = @StreetAddress
	,City = @city
	,StateAddress = @venuestate
	,ZIPCode = @ZIPCode
	,ContactName = @ContactName
	,ContactPhone = @ContactPhone
	,ContactEmail = @ContactEmail
Where VenueID = @VenueID