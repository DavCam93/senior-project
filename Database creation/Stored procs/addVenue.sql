CREATE PROCEDURE usp_AddVenue
		 @Name			nvarchar(255)
		,@StreetAddress	nvarchar(255)	=	null
		,@city			nvarchar(255)	=	null
		,@venuestate	nchar(2)		=	null
		,@ZIPCode		nchar(5)		=	null
		,@ContactName	nvarchar(255)	=	null
		,@ContactPhone	nchar(13)		=	null
		,@ContactEmail	nchar(255)	=	null
AS

INSERT INTO VENUE(VenueName,StreetAddress,City,StateAddress,ZIPCode,ContactName,ContactPhone,ContactEmail)
VALUES(@Name,@StreetAddress,@city,@venuestate,@ZIPCode,@ContactName,@ContactPhone,@ContactEmail)