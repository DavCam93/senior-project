CREATE PROCEDURE usp_EditEvent
	@EventID			INT
	,@EventName			nvarchar(255)
	,@EventDescription	nvarchar(max)
	,@REquipment		nvarchar(max)
	,@RoomConfig		INT
	,@ShowID			int
	,@StartTime			datetime2(0)
	,@EndTime			datetime2(0)
	,@ExpectedAttendance	int
	,@ActualAttendance		int
	,@WasInPreviousShow		bit

AS

	UPDATE SHOW_EVENT
	SET
	EventName = @EventName
	,EventDescription = @EventDescription
	,RequestedEquipment = @REquipment
	,RoomConfiguration = @RoomConfig
	,ShowID = @ShowID
	,StartTime = @StartTime
	,EndTime = @EndTime
	,ExpectedAttendance = @ExpectedAttendance
	,ActualAttendance = @ActualAttendance
	,WasInPreviousShow = @WasInPreviousShow
	WHERE EventID = @EventID