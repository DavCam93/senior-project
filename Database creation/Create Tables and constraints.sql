USE ShowStarterDB

CREATE TABLE USERS (
	 UserID			int				NOT NULL IDENTITY(1,1)
	,UserName		nvarchar(50)	NOT NULL
	,UserPassword	varbinary(512)	NOT NULL
	,Salt			nchar(25)		NOT NULL
	,LegalFirstName	nvarchar(50)	NOT NULL
	,LegalLastName	nvarchar(50)	NOT NULL
	,PhoneNumber	nchar(13)		NULL
	,Email			nvarchar(255)	NOT NULL
	,StreetAddress	nvarchar(255)	NULL
	,City			nvarchar(255)   NULL
	,StateAddress	nchar(2)		NULL
	,ZIPCode		nchar(5)		NULL
	,

	 CONSTRAINT		PK_UserID		PRIMARY KEY(UserID)
	,CONSTRAINT		CK_UserPhone	CHECK(PhoneNumber LIKE '([1-9][1-9][1-9])[1-9][1-9][1-9]-[1-9][1-9][1-9][1-9]')
	,CONSTRAINT		CK_UserZIP		CHECK(ZIPCode LIKE '[1-9][1-9][1-9][1-9][1-9]')
	,CONSTRAINT		CK_UserState	Check(StateAddress LIKE '[A-Z][A-Z]')
	,CONSTRAINT		AK_UserName		UNIQUE(UserName)
	,	
);

CREATE TABLE SHOW (
	 ShowID			int				NOT NULL IDENTITY(1,1)
	,ShowName		nvarchar(255)	NOT NULL
	,StartDate		datetime2(0)	NULL
	,EndDate		datetime2(0)	NULL
	,Theme			nvarchar(255)	NULL
	,isDefaultShow	bit				NOT NULL DEFAULT('0')
	,

	 CONSTRAINT		PK_ShowID		PRIMARY KEY(ShowID)
	,
);

CREATE TABLE VENUE (
	 VenueID			int					NOT NULL IDENTITY(1,1)
	,VenueName			nvarchar(255)		NOT NULL
	,StreetAddress		nvarchar(255)		NULL
	,City				nvarchar(255)		NULL
	,StateAddress		nchar(2)			NULL
	,ZIPCode			nchar(5)			NULL
	,ContactName		nvarchar(255)		NULL
	,ContactPhone		nchar(13)			NULL
	,ContactEmail		nvarchar(255)		NULL
	,
	
	 CONSTRAINT		PK_VenueID		PRIMARY KEY(VenueID)
	,CONSTRAINT		CK_VenuePhone	CHECK(ContactPhone LIKE '([1-9][1-9][1-9])[1-9][1-9][1-9]-[1-9][1-9][1-9][1-9]')
	,CONSTRAINT		CK_VenueZIP		CHECK(ZIPCode LIKE '[1-9][1-9][1-9][1-9][1-9]')
	,CONSTRAINT		CK_VenueState	Check(StateAddress LIKE '[A-Z][A-Z]')
	,		
);

CREATE TABLE DEPARTMENT (
	 DepartmentID			int				NOT NULL IDENTITY(1,1)
	,DepartmentName			nvarchar(255)	NOT NULL
	,DeparmentDescription	nvarchar(max)	NULL
	,

	 CONSTRAINT				PK_DeptID	PRIMARY KEY(DepartmentID)
	,
);

CREATE TABLE RENTAL_COMPANY (
	 RentalCompanyID			int					NOT NULL IDENTITY(1,1)
	,RentalCompanyName			nvarchar(255)		NOT NULL
	,StreetAddress				nvarchar(255)		NULL
	,City						nvarchar(255)		NULL
	,StateAddress				nchar(2)			NULL
	,ZIPCode					nchar(5)			NULL
	,ContactName				nvarchar(255)		NULL
	,ContactPhone				nchar(13)			NULL
	,ContactEmail				nvarchar(255)		NULL
	,
	
	 CONSTRAINT		PK_RentalCompanyID		PRIMARY KEY(RentalCompanyID)
	,CONSTRAINT		CK_RentalCompanyPhone	CHECK(ContactPhone LIKE '([1-9][1-9][1-9])[1-9][1-9][1-9]-[1-9][1-9][1-9][1-9]')
	,CONSTRAINT		CK_RentalCompanyZIP		CHECK(ZIPCode LIKE '[1-9][1-9][1-9][1-9][1-9]')
	,CONSTRAINT		CK_RentalCompanyState	Check(StateAddress LIKE '[A-Z][A-Z]')
	,		
);

CREATE TABLE TEXT_ALERT (
	 TextAlertID		int				NOT NULL IDENTITY(1,1)
	,AlertMessage		nvarchar(160)	NOT NULL
	,ScheduledSendTime	datetime2(0)	NOT NULL
	,SentTime			datetime2(0)	NULL
	,

	 CONSTRAINT		PK_TextAlert		PRIMARY KEY(TextAlertID)
	,
);


CREATE TABLE TRACK (
	 TrackID			int				NOT NULL IDENTITY(1,1)
	,TrackName			nvarchar(255)	NOT NULL
	,TrackDescription	nvarchar(max)	NULL
	,

	 CONSTRAINT		PK_Track	PRIMARY KEY(TrackID)
	,

);

CREATE TABLE BADGE_TYPE (
	 BadgeTypeID		int				NOT NULL IDENTITY(1,1)
	,BadgeName			nvarchar(50)	NOT NULL
	,BadgePrice			smallmoney		NOT NULL
	,IsAvailable		bit				NOT NULL DEFAULT(0)
	,

	CONSTRAINT			PK_BadgeType	PRIMARY KEY(BadgeTypeID)
);


--Past this point be dragons, and forign key constraints.


CREATE TABLE SHOW_VENUES (
	 ShowID			int			NOT NULL
	,VenueID		int			NOT NULL
	,

	 CONSTRAINT		FK_SV_ShowID	FOREIGN KEY(ShowID)
									REFERENCES SHOW(ShowID)
	,CONSTRAINT		FK_SV_VenueID	FOREIGN KEY(VenueID)
									REFERENCES VENUE(VenueID)
	,
);

CREATE TABLE SHOW_DEPARTMENTS (
	 ShowID				int			NOT NULL
	,DepartmentID		int			NOT NULL
	,

	 CONSTRAINT		FK_SD_ShowID		FOREIGN KEY(ShowID)
										REFERENCES SHOW(ShowID)
	,CONSTRAINT		FK_SD_DepartmentID	FOREIGN KEY(DepartmentID)
										REFERENCES DEPARTMENT(DepartmentID)
	,
);

CREATE TABLE INVENTORY_ITEM (
	 InventoryID		int				NOT NULL IDENTITY(1,1)
	,ShortDescription	nvarchar(255)	NOT NULL
	,LongDescription	nvarchar(max)	NULL
	,QuantityOnHand		int				NOT NULL DEFAULT(1)
	,Department			int				NOT NULL
	,

	 CONSTRAINT		PK_InventoryID		PRIMARY KEY(InventoryID)
	,CONSTRAINT		FK_II_Department	FOREIGN KEY(Department)
										REFERENCES DEPARTMENT(DepartmentID)
	,
);

CREATE TABLE RENTAL_ITEMS (
	 InventoryID		int		NOT NULL
	,RentalCompanyID	int		NOT NULL
	,

	 CONSTRAINT	FK_RI_InventoryID		FOREIGN KEY(InventoryID) REFERENCES INVENTORY_ITEM(InventoryID)
	,CONSTRAINT	FK_RI_RentalCompanyID	FOREIGN KEY(RentalCompanyID) REFERENCES RENTAL_COMPANY(RentalCompanyID)
	,
);

CREATE TABLE STAFF_POSITION (
	 PositionID				int				NOT NULL IDENTITY(1,1)
	,DepartmentID			int				NOT NULL
	,PositionName			nvarchar(50)	NOT NULL
	,PositionDescription	nvarchar(max)	null
	,

	 CONSTRAINT PK_PositionID		PRIMARY KEY(PositionID)
	,CONSTRAINT	FK_SP_DepartmentID	FOREIGN KEY(DepartmentID) REFERENCES DEPARTMENT(DepartmentID)
	,

);

CREATE TABLE USER_POSITIONS (
	 PositionID		int		NOT NULL
	,UserID			int		NOT NULL
	,

	 CONSTRAINT	FK_UP_PositionID	FOREIGN KEY(PositionID) REFERENCES STAFF_POSITION(PositionID)
	,CONSTRAINT	FK_UP_UserID		FOREIGN KEY(UserID) REFERENCES USERS(UserID)
	,
);

CREATE TABLE USER_ALERTS(
	 AlertID		int		not null
	,UserID			int		not null
	,

	 CONSTRAINT	FK_UA_UserID	FOREIGN KEY(UserID) REFERENCES USERS(UserID)
	,CONSTRAINT	FK_UA_AlertID	FOREIGN KEY(AlertID) REFERENCES TEXT_ALERT(TextAlertID)
	,
);


CREATE TABLE ROOM(
	 RoomID				int				NOT NULL IDENTITY(1,1)
	,VenueID			int				NOT NULL
	,Name				nvarchar(50)	NOT NULL
	,IsSinglePurpose	bit				NOT NULL DEFAULT(0)
	,MainPurpose		nvarchar(max)	NULL
	,IsCombinedRoom		bit				NOT NULL DEFAULT(0)
	,

	 CONSTRAINT	PK_RoomID		PRIMARY KEY(RoomID)
	,CONSTRAINT FK_Room_Venue	FOREIGN KEY(VenueID) REFERENCES VENUE(VenueID)
	,

);


/*  We'll get back to this eventually
CREATE TABLE COMBINED_ROOM(
	 RoomCreated		int		NOT NULL
	,RoomUsed			int		NOT NULL
	,

	 CONSTRAINT FK_CR_RoomCreated	FOREIGN KEY(RoomCreated) REFERENCES ROOM(RoomID)
	,CONSTRAINT CK_RoomCreates		CHECK(room Select isCombinedRoom (where RoomCreated = RoomID) == 0)
	,
);
*/

CREATE TABLE ROOM_CONFIGURATION (
	 ConfigurationID		int				NOT NULL IDENTITY(1,1)
	,RoomID					int				NOT NULL
	,ConfigurationName		nvarchar(50)	NOT NULL
	,ConfigDescription		nvarchar(max)	NULL
	,Capacity				int				NOT NULL DEFAULT(0)
	,EstimatedSwitchTime	time			NOT NULL
	,

	 CONSTRAINT	PK_ConfigurationID		PRIMARY KEY(ConfigurationID)
	,CONSTRAINT	FK_RConf_RoomID			FOREIGN KEY(RoomID) REFERENCES ROOM(RoomID)
	,
);

CREATE TABLE SHOW_EVENT (
	 EventID				int				NOT NULL IDENTITY(1,1)
	,EventName				nvarchar(255)	NOT NULL
	,EventDescription		nvarchar(max)	NULL
	,RequestedEquipment		nvarchar(max)	NULL
	,RoomConfiguration		int				NULL
	,ShowID					int				NOT NULL
	,StartTime				datetime2(0)	NULL
	,EndTime				datetime2(0)	NULL
	,ExpectedAttendance		int				NOT NULL DEFAULT(0)
	,ActualAttendance		int				NULL
	,WasInPreviousShow		bit				NOT NULL DEFAULT(0)
	,

	 CONSTRAINT PK_EventID				PRIMARY KEY(EventID)
	,CONSTRAINT	FK_Event_RoomConfig		FOREIGN KEY(RoomConfiguration) REFERENCES ROOM_CONFIGURATION(ConfigurationID)
	,CONSTRAINT	FK_Event_Show			FOREIGN KEY(ShowID) REFERENCES	SHOW(ShowID)
	,
);

CREATE TABLE EVENT_TRACKS(
	 EventID		int		NOT NULL
	,TrackID		int		NOT NULL
	,

	 CONSTRAINT	FK_ET_EventID	FOREIGN KEY(EventID) REFERENCES SHOW_EVENT(EventID)
	,CONSTRAINT	FK_ET_TrackID	Foreign KEY(TrackID) REFERENCES TRACK(TrackID)
	,
);

CREATE TABLE EVENT_PANELISTS(
	 EventID		int		NOT NULL
	,PanelistID		int		NOT NULL
	,

	 CONSTRAINT	FK_EP_EventID		FOREIGN KEY(EventID) REFERENCES SHOW_EVENT(EventID)
	,CONSTRAINT	FK_EP_PanelistID	FOREIGN KEY(PanelistID) REFERENCES USERS(UserID)
	,
);

CREATE TABLE USER_SCHEDULE(
	 EventID		int		NOT NULL
	,UserID			int		NOT NULL
	,

	 CONSTRAINT	FK_US_EventID		FOREIGN KEY(EventID) REFERENCES SHOW_EVENT(EventID)
	,CONSTRAINT	FK_US_UserID	FOREIGN KEY(UserID) REFERENCES USERS(UserID)
	,
);

CREATE TABLE BADGE(
	 BadgeID		int				NOT NULL IDENTITY(1,1)
	,ShowID			int				NOT NULL
	,PurchaserID	int				NOT NULL
	,BadgeType		int				NOT NULL
	,LineOneInfo	nvarchar(50)	NOT NULL
	,LineTwoInfo	nvarchar(50)	NULL
	,

	 CONSTRAINT PK_Badge			PRIMARY KEY(BadgeID)
	,CONSTRAINT	FK_BADGE_ShowID		FOREIGN KEY (ShowID) REFERENCES SHOW(ShowID)
	,CONSTRAINT	FK_BADGE_Purchaser	FOREIGN KEY (PurchaserID) REFERENCES USERS(UserID)
	,CONSTRAINT	FK_BADGE_BadgeType	FOREIGN KEY(BadgeType) REFERENCES BADGE_TYPE(BadgeTypeID)
	,
);

CREATE TABLE GROUPS (
	 GroupID			nvarchar(5)		NOT NULL
	,GroupDescription	nvarchar(255)	null
	,

	CONSTRAINT PK_Group			PRIMARY KEY(GroupID)
	,
);


CREATE TABLE USER_GROUPS (
	 GroupID			nvarchar(5)		NOT NULL
	,UserID				int				NOT NULL
	,

	 CONSTRAINT FK_UG_GroupID	FOREIGN KEY (GroupID)	References GROUPS(GroupID)
	,CONSTRAINT	FK_UG_UserID	FOREIGN KEY (UserID)	REFERENCES USERS(USerID)
);